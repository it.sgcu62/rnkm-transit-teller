FROM golang:1.12
LABEL maintainer ["Tiger T. Pisnupoomi", "Roadchananat Khunakornophat"]
WORKDIR /go/src/github.com/it-sgcu/rnkm-transit/teller
COPY . .
RUN go get ./...
RUN go build -o Teller
CMD ["./Teller"]
