package broker

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/it-sgcu/rnkm-transit/teller/constant"
	message "github.com/it-sgcu/rnkm-transit/teller/model/message"
	"github.com/pkg/errors"
	"github.com/segmentio/kafka-go"
)

// KafkaBroker simple implementation of Kafka (no partiton yet)
type KafkaBroker struct {
	transitResProducer *kafka.Writer
	transitReqConsumer *kafka.Reader
	callback           chan *message.TransitRequest
	balancer           kafka.Balancer
	logger             *log.Logger
	stop               bool
	addr               []string
}

// NewKafkaBroker : create kafka broker
func NewKafkaBroker(address []string) *KafkaBroker {
	b := &KafkaBroker{
		transitReqConsumer: kafka.NewReader(kafka.ReaderConfig{
			Brokers:  address,
			Topic:    "tranReq",
			GroupID:  "teller",
			MinBytes: 5,
			MaxBytes: 10e6,
		}),
		stop:     false,
		addr:     address,
		logger:   log.New(os.Stdout, "[Broker]", log.LstdFlags),
		balancer: &constant.TransitBalancer{},
	}
	b.transitResProducer = kafka.NewWriter(kafka.WriterConfig{
		Brokers:      address,
		Topic:        "tranRes",
		Balancer:     b.balancer,
		RequiredAcks: 1,
		BatchTimeout: 10 * time.Millisecond,
	})
	return b
}

func truncate(s []byte, length int) []byte {
	if len(s) < length {
		return s
	}
	return s[:length]
}

func (b *KafkaBroker) logError(msg string, err error) {
	b.logger.SetOutput(os.Stderr)
	b.logger.Println(msg)
	if err != nil {
		b.logger.Printf("%+v\n", errors.WithStack(err))
	}
	b.logger.SetOutput(os.Stdout)
}

// Start : Start Writer/Reader loop; should be called when sub's are ready so they doesn't miss message
func (b *KafkaBroker) Start() {
	b.startWatchTransitRequest()
}

// Stop : Close all writers, consumer
func (b *KafkaBroker) Stop() {
	b.transitReqConsumer.Close()
	b.stop = true
}

func (b *KafkaBroker) startWatchTransitRequest() {
	go func() {
		for !b.stop {
			var transitRequest message.TransitRequest
			m, err := b.transitReqConsumer.ReadMessage(context.Background())
			if err != nil {
				panic("TransitRequestConsumerError : " + err.Error())
			}
			json.Unmarshal(m.Value, &transitRequest)
			fmt.Printf("On %d\n", m.Partition)
			b.callback <- &transitRequest
		}
	}()
}

func (b *KafkaBroker) SubscribeTransitRequest(receiver chan *message.TransitRequest) {
	b.callback = receiver
}

// Publish : publish message with value: `value` in to topic `topic` (I might add key in future ?)
func (b *KafkaBroker) Publish(topic, key string, value []byte) error {
	return b.transitResProducer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(key),
			Value: value,
		},
	)
}
