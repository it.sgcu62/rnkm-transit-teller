package broker

import (
	"github.com/it-sgcu/rnkm-transit/teller/model/message"
)

// Broker : transit & population message broker
type Broker interface {
	Publish(topic, key string, message []byte) error // Publish to broker

	SubscribeTransitRequest(receiver chan *message.TransitRequest) // Sub to broker
}
