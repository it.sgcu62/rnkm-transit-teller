package constant

import (
	"github.com/kelseyhightower/envconfig"
)

type Configuration struct {
	RedisHost    string   `default:"redis:6379" split_words:"true"`
	KafkaBrokers []string `default:"kafka:9092" split_words:"true"`
}

var Config Configuration

func LoadConfig() {
	envconfig.Process("TELLER", &Config)
}
