package db

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	modelBaan "github.com/it-sgcu/rnkm-transit/teller/model/baan"

	"github.com/go-redis/redis"
	constant "github.com/it-sgcu/rnkm-transit/teller/constant"
)

const STRING_WIDTH = 3 // Width of enconding in parseStringArray and encodeStringArray

type Redis struct {
	client *redis.Client
	logger *log.Logger
}

// NewRedis create new redis client with connection to `broker`
func NewRedis() *Redis {
	return &Redis{
		logger: log.New(os.Stdout, "[Population]", log.Lshortfile),
	}
}

// prepareDB ensure that all baan's are in HASH and is initialized
func (r *Redis) prepareDB() error {
	batch := r.client.Pipeline()
	facultyZero := "" // represent all zero in each faculty
	for i := 0; i < len(constant.FacultyList); i++ {
		facultyZero += "  0"
	}
	for _, baan := range constant.BaanList {
		batch.HSetNX("population/"+baan, "limit", constant.MockBaanLimit[baan])
		batch.HSetNX("population/"+baan, "current", "0")
		batch.HSetNX("population/"+baan, "faculty", facultyZero) // 3 digit each
		batch.HSetNX("population/"+baan, "gender", "  0  0")     // 0 female and 0 male
	}
	_, err := batch.Exec()
	if err != nil {
		r.logger.Printf("Error initializing database %v\n", err)
	} else {
		r.logger.Println("Init database OK")
	}
	return err
}

func (r *Redis) Init(rOpt *redis.Options) {
	r.client = redis.NewClient(rOpt)
	// r.client = redis.NewFailoverClient(&redis.FailoverOptions{
	// 	MasterName:    "mymaster",
	// 	SentinelAddrs: []string{rOpt.Addr},
	// })
	pong, err := r.client.Ping().Result()
	if err != nil {
		// r.logger.Printf("[FATAL] can't connect to redis on %s\n", rOpt)
		r.logger.Printf("error: %v\n", err)
		os.Exit(1)
	}
	r.logger.Printf("%s\n", pong)
}

// parseInt: helper to parseInt with spaces leading/behind
func parseInt(str string) (int, error) {
	trimmed := strings.Trim(str, " \t\n")
	return strconv.Atoi(trimmed)
}

// mapJoin = apply mapFunc to each string
func encodeStringArray(array []int, width int) string {
	format := fmt.Sprintf("%%0%dd", width) // %0<width>d
	str := ""
	for _, v := range array {
		str += fmt.Sprintf(format, v)
	}
	return str
}

func parseStringArray(str string, width int) ([]int, error) {
	if len(str)%width != 0 {
		return nil, errors.New("Parsing error []")
	}
	arr := make([]int, len(str)/width)
	for i := range arr {
		num, parseErr := parseInt(str[width*i : width*(i+1)])
		if parseErr != nil {
			return nil, errors.New(fmt.Sprintf("Parse error at %d:%d - [%s] is not int\n", width*i, width*(i+1), str[width*i:width*(i+1)]))
		}
		arr[i] = num
	}
	return arr, nil
}

// parseRedisBaan: convert redis style `map[string]string` to `Population` struct
func parseRedisBaan(baan map[string]string) (population modelBaan.Population, err error) {
	var faculty []int
	var gender []int
	var limit, current int

	limit, err = parseInt(baan["limit"])
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing limit baan:%s\n offending value = [%s]\n", err, baan["limit"]))
		return
	}
	population.Limit = limit

	current, err = parseInt(baan["current"])
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing current baan:%s\n offending value = [%s]\n", err, baan["current"]))
		return
	}
	population.Current = current

	faculty, err = parseStringArray(baan["faculty"], STRING_WIDTH)
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing faculty baan:%s\n offending value = [%s]\n", err, baan["faculty"]))
		return
	}
	population.Faculty = faculty

	gender, err = parseStringArray(baan["gender"], STRING_WIDTH)
	if err != nil {
		err = errors.New(fmt.Sprintf("Error Parsing gender baan:%s\n offending value = [%s]\n", err, baan["gender"]))
		return
	}
	population.Gender[0] = gender[0]
	population.Gender[1] = gender[1]

	return
}

// BeginTransaction for student from xx to dstBaan
func (r *Redis) BeginTransaction(fn func(*redis.Tx) error, studentID, dstBaan string) error {
	return r.client.Watch(fn, studentID, dstBaan)
}

// HIncrBaan : set redis data of `baan` according to `population`
func HIncrBaan(pipe redis.Pipeliner, baan string, diff int64) error {
	_, err := pipe.HIncrBy("population/"+baan, "current", diff).Result()
	return err
}

// HSetBaan : set redis data of `baan` according to `population`
func HSetBaan(pipe redis.Pipeliner, baan string, population modelBaan.Population) error {
	_, err := pipe.HMSet("population/"+baan, map[string]interface{}{
		// "limit":   population.Limit,
		// "current": population.Current,
		"faculty": encodeStringArray(population.Faculty, STRING_WIDTH),
		"gender":  encodeStringArray(population.Gender[:2], STRING_WIDTH),
	}).Result()
	return err
}

func GetBaan(tx *redis.Tx, baan string) (modelBaan.Population, error) {
	data, err := tx.HGetAll("population/" + baan).Result()
	if err != nil {
		return modelBaan.Population{}, err
	} else {
		population, err := parseRedisBaan(data)
		if err != nil {
			return modelBaan.Population{}, err
		}
		return population, nil
	}
}

func SetStudentBaan(pipe redis.Pipeliner, id, baan string) error {
	_, err := pipe.HSet("student/"+id, "baan", baan).Result()
	return err
}

// TODO: optimize to allow setting only partial of baan (that's why I encoded like this)
