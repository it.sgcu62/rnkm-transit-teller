package main

import (
	"github.com/go-redis/redis"
	"github.com/it-sgcu/rnkm-transit/teller/broker"
	"github.com/it-sgcu/rnkm-transit/teller/constant"
	"github.com/it-sgcu/rnkm-transit/teller/db"
	"github.com/it-sgcu/rnkm-transit/teller/teller"
)

func main() {
	constant.LoadConfig()
	redisConn := db.NewRedis()
	redisConn.Init(&redis.Options{
		Addr:     constant.Config.RedisHost,
		Password: "",
		DB:       0,
	})
	kafkaBroker := broker.NewKafkaBroker(constant.Config.KafkaBrokers)
	kafkaBroker.Start()
	t := teller.NewTeller(redisConn, kafkaBroker)
	t.Run()
}
