package baan

// Population : Population of baan
type Population struct {
	Limit   int    `json:"l"`
	Current int    `json:"c"`
	Faculty []int  `json:"f"`
	Gender  [2]int `json:"g"`
}

// WorldPopulations : Population for each baan
type WorldPopulations map[string]Population
