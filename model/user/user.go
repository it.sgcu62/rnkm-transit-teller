package user

// NationalCard : Freshmen student info
type NationalCard struct {
	UserID  string `json:"id"`
	Faculty int    `json:"faculty"`
	Gender  int    `json:"gender"`
}

// DrivingLicense : Transit session
type DrivingLicense struct {
	NationalCard `json:"nationality"`
	ID           string `json:"id"` // randomized for ensure that session is unique
	Address      string `json:"addr"`
}
