package teller

import (
	"encoding/json"
	"errors"
	"log"
	"os"

	"github.com/go-redis/redis"
	"github.com/it-sgcu/rnkm-transit/teller/broker"
	"github.com/it-sgcu/rnkm-transit/teller/constant"
	"github.com/it-sgcu/rnkm-transit/teller/db"
	"github.com/it-sgcu/rnkm-transit/teller/model/message"
	"github.com/it-sgcu/rnkm-transit/teller/model/user"
)

type Teller struct {
	db      *db.Redis
	broker  broker.Broker
	channel chan *message.TransitRequest
	logger  *log.Logger
}

// NewTeller create new teller connected to `redis` (for database) and `broker` subbing to transit
func NewTeller(redis *db.Redis, broker broker.Broker) *Teller {
	return &Teller{
		db:      redis,
		broker:  broker,
		channel: make(chan *message.TransitRequest, 100),
		logger:  log.New(os.Stdout, "[Teller]", log.LstdFlags),
	}
}

// Run subs to broker and start listening for "transit message"
func (t *Teller) Run() {
	t.broker.SubscribeTransitRequest(t.channel)
	for {
		t.processTransitRequest(<-t.channel)
	}
}

// processTransitRequest: process `message.TransitRequest` and publish result
func (t *Teller) processTransitRequest(request *message.TransitRequest) {
	// copy old code here
	result := message.TransitResult{
		DrivingLicense: user.DrivingLicense{
			Address:      request.To,
			ID:           request.DrivingLicense.ID,
			NationalCard: request.NationalCard,
		},
		Reason: "200",
	}
	userNationalCard := request.DrivingLicense.NationalCard

	defer func() {
		msg, _ := json.Marshal(result)
		t.broker.Publish("tranRes", request.DrivingLicense.Address, msg)
	}()

	if request.DrivingLicense.Address == request.To {
		result.Reason = "304"
		return
	}

	if _, exist := constant.MockBaanLimit[request.To]; !exist {
		result.Reason = "404"
		return
	}

	err := t.db.BeginTransaction(
		func(tx *redis.Tx) (err error) {
			dstPopulation, err := db.GetBaan(tx, request.To)
			if err == nil && dstPopulation.Current < dstPopulation.Limit {
				srcPopulation, err := db.GetBaan(tx, request.DrivingLicense.Address) // WARNING: old baan might not be valid anymore
				if err != nil {                                                      // Can't get detail of old baan for some reason
					return errors.New("500, " + err.Error())
				}

				srcPopulation.Gender[userNationalCard.Gender]--
				srcPopulation.Faculty[userNationalCard.Faculty]--
				// srcPopulation.Current--

				dstPopulation.Gender[userNationalCard.Gender]++
				dstPopulation.Faculty[userNationalCard.Faculty]++
				// dstPopulation.Current++

				_, err = tx.Pipelined(
					func(pipe redis.Pipeliner) error {
						db.HIncrBaan(pipe, request.To, 1)
						db.HIncrBaan(pipe, request.DrivingLicense.Address, -1)
						
						db.HSetBaan(pipe, request.To, dstPopulation)
						db.HSetBaan(pipe, request.DrivingLicense.Address, srcPopulation)
						db.SetStudentBaan(pipe, request.DrivingLicense.NationalCard.UserID, request.To)
						return nil
					},
				)
				if err != nil {
					println("409 ", err.Error())
					return errors.New("409")
				}
				return nil
			}
			if err != nil {
				println("500 ", err.Error())
				return errors.New("500")
			}
			return errors.New("418")
		},
		request.DrivingLicense.NationalCard.UserID, request.To)
	if err != nil {
		result.Reason = err.Error()
	}
	t.logger.Printf("Process licenseID #%s(%s) request to '%s' with status '%s'", request.ID, request.NationalCard.UserID, request.To, result.Reason)
}
